#!/usr/bin/env python
# This Source Code Form is subject to the terms of the
# Mozilla Public License, v. 2.0. If a copy of the MPL
# was not distributed with this file, You can obtain one
# at https://mozilla.org/MPL/2.0/.

"""
Steam Deck (Jupiter) BIOS Tool

Automatically analyze, verify, dump/generate/inject UID, and dynamically trim any/all
Steam Deck (jupiter) BACKUP and RELEASE BIOS (*_sign.fd) (>0x1000000) images to 0x1000000
for hardware flashing/programming to a Winbond W25Q128JW 128Mb (16MB) Serial NOR Flash.
"""

# minimum      :  Python 2.6+/3.1+ (requires backported module: argparse)
# recommended  :  Python 2.7+/3.2+

# pylint: disable=C

import argparse
import base64
import os
import random
import string
import sys

# version
rel_bios_ver = 'F7A0118'
# compare/match offset(s) from bios_offset_list
detect_rel_bios_ver = None
# dynamic/detected BIOS image version/offset
bios_ver = None
bios_ver_offset = None
# dynamic/detected BIOS image version offset - overrides bios_ver/bios_ver_offset
bios_ver_offset_str = b'CHACHANI-VANGOGH'
bios_ver_offset_str_negative_offset = 0x19
bios_ver_size = 0x7
# dynamic/detected BIOS image revision/date/offset
bios_rev = None
bios_rev_offset = None
bios_date = None
bios_date_offset = None
# dynamic/detected BIOS image revision/date offset - overrides bios_rev/bios_rev_offset/bios_date/bios_date_offset
#bios_rev_offset_str = b'Jupiter'
bios_rev_offset_str = b'F7A Jupiter'
#bios_rev_offset_str_negative_offset = 0x1d
bios_rev_offset_str_negative_offset = 0x19
bios_rev_size = 0x6
#bios_date_offset_str_negattive_offset = 0xf
bios_date_offset_str_negative_offset = 0xb
bios_date_size = 0xa

# header
#rel_header = b'\x4d\x5a'
rel_header = b'MZ'
bios_header = b'\x02\x02\x00\x02\x28'

# offset
# RELEASE BIOS byte size list - [byte_size_int, str]
rel_bios_size_list = [
    0x10f48c8, 'F7A0102-%s(-???????) (F7A0102+)' % rel_bios_ver,
    0x1188908, 'Test2 (Test2_sign.fd)/F7A0006-F7A0101',
    0x11888d8, 'Test1 (Test1_sign.fd)']
# BIOS offset list - [offset_int, str]
bios_offset_list = [
    0xe8c70, 'F7A0102-%s(-???????) (F7A0102+)' % rel_bios_ver,
    0x17d050, 'F7A0101 or lower (F7A0101-)']
# dynamic/detected BIOS offset
#bios_offset = None
bios_offset = bios_offset_list[0]
# dynamic/detected BIOS offset - overrides bios_offset
#bios_offset_str = b'UUITE Tech. Inc.  '
bios_offset_str = b'UUITE Tech. Inc.'
bios_offset_str_negative_offset = 0x4e
bios_size = 0x1000000
# dynamic/detected UID/offset/size
bios_uid = None
#bios_uid_offset = 0x6a4000
bios_uid_offset = None
# dynamic/detected UID offset - overrides bios_uid/bios_uid_offset/bios_uid_size
#bios_uid_offset_str = b'\x24\x44\x4d\x49'
bios_uid_offset_str = b'$DMI'
#bios_uid_size = 0x174
bios_uid_size = None

# download/network
rel_bios_dl_pkg = ['zip', 'tar.gz', 'tar.bz2', 'tar']
#rel_bios_dl_pkg_nozip = [s for s in rel_bios_dl_pkg if s != 'zip']
rel_bios_dl_pkg_nozip = ['tar.gz', 'tar.bz2', 'tar']
src_form = ['blob', 'raw']

# generate UID
# set to override pseudorandom generation (one/single CAPITAL/UPPERCASE alpha/letter) - MEB[?]XXXXXXXX
mebx_letter = None
# set to override pseudorandom generation (8 digit integer/number) - MEBX[????????]
mebx_number = None
# set to override pseudorandom generation (8 byte alphanumeric) - MEBX[????????]
mebx_alphanumeric = None
# serial number - set to override pseudorandom generation (one/single CAPITAL/UPPERCASE alpha/letter) - F[?]AAXXXXXXXX
fxaa_letter = None
# serial number - set to override pseudorandom generation (8 digit integer/number) - FXAA[????????]
fxaa_number = None
# serial alphanumeric - set to override pseudorandom generation (8 byte alphanumeric) - FXAA[????????]
fxaa_alphanumeric = None
uid_1 = '0x00,0x00,0x00,0x00-0x00,0x00,0x00,0x00-0x00,0x00,0x00,0x00'
#uid_1_null = b'\xff' * len(uid_1)
uid_1_null = b'\xff' * 59
uid_2 = '0x00,0x00,0x00,0x00-0x00,0x00,0x00,0x00-0x00,0x00,0x00,0x00'
#uid_2_null = b'\xff' * len(uid_2)
uid_2_null = b'\xff' * 59
dump_uid_default_file = 'dumped_jupiter-UID.bin'
generate_uid_default_file = 'generated_jupiter-UID.bin'

# info/misc
# RELEASE BIOS UID offset list - [byte_size_int, str]
rel_bios_uid_offset_list = [
    0x78cc70, 'F7A0102-%s(-???????) (F7A0102+)' % rel_bios_ver,
    0x821050, 'F7A0101 or lower (F7A0101-)']
# block/string is duplicated/repeated at +0x800000 - [offset_int, offset_int + 0x800000]
rel_bios_ver_offset_list = [0x790c7e, 0xf90c7e]
bios_ver_offset_list = [0x6a800e, 0xea800e]
# block/string is duplicated/repeated at +0x40000 - [offset_int, offset_int + 0x40000]
rel_bios_rev_offset_list = [0xf0c76, 0x130c76]
bios_rev_offset_list = [0x8006, 0x48006]
rel_bios_date_offset_list = [0xf0c84, 0x130c84]
bios_date_offset_list = [0x8014, 0x48014]
invalid = None
name_list = ['chachani', 'jupiter', 'steamdeck', 'vangogh']
rcl = ['~', '#', '%', '^', '&', '*', '-', '_', '=', '+', '\\', '|', "'", '"', ':', '.', '/']
rc = random.choice(rcl)

# title
print("\n  Steam Deck (jupiter) BIOS Tool v0.3\n %s (jupiter-bios-tool) %s\n Copyright (C) 2022-2023 %s\n" % (rc * 8, rc * 8, base64.b64decode('RHJha2UgU3RlZmFuaQ==').decode('utf-8')))
if '-h' in sys.argv or '--help' in sys.argv:
    print("-" * 12)
    print("Automatically analyze, verify, dump/generate/inject UID, and dynamically trim any/all Steam Deck (jupiter) BACKUP and RELEASE BIOS (*_sign.fd) (>0x1000000) images to 0x1000000 for hardware flashing/programming to a Winbond W25Q128JW 128Mb (16MB) Serial NOR Flash.")
    print("-" * 12)
    print()
    print("=" * 24)
    print("Source Code:")
    for s in src_form:
        print("  https://gitlab.com/evlaV/jupiter-PKGBUILD/-/%s/master/jupiter-bios-tool.py" % s)
    print("=" * 24)
    print("Documentation:\n  https://gitlab.com/evlaV/jupiter-PKGBUILD#-steam-deck-jupiter-bios-backup-\n  https://gitlab.com/evlaV/jupiter-PKGBUILD#-steam-deck-jupiter-bios-tool-jupiter-bios-tool-")
    print("=" * 24)
    print("RELEASE BIOS Database:\n  https://gitlab.com/evlaV/jupiter-PKGBUILD#valve-official-steam-deck-jupiter-release-bios-database")
    print("=" * 24)
    print("RELEASE BIOS Download:\n  https://gitlab.com/evlaV/jupiter-hw-support/-/tree/master/usr/share/jupiter_bios\n  https://gitlab.com/evlaV/jupiter-hw-support/-/archive/master/jupiter-hw-support-master.%s?path=usr/share/jupiter_bios" % random.choice(rel_bios_dl_pkg))
    print("-" * 12)
    print("Download current/latest RELEASE BIOS to %s/jupiter_bios/ - requires curl and bsdtar (libarchive):" % os.getcwd())
    print("  $ curl https://gitlab.com/evlaV/jupiter-hw-support/-/archive/master/jupiter-hw-support-master.%s?path=usr/share/jupiter_bios | bsdtar --strip-components=3 -xvf-" % random.choice(rel_bios_dl_pkg))
    print("=" * 24)
    print("BIOS Backup:")
    print("If running official (jupiter) SteamOS - requires jupiter-hw-support:")
    print("  $ sudo /usr/share/jupiter_bios_updater/h2offt jupiter-%s-bios-backup.bin -O" % rel_bios_ver)
    print("-" * 12)
    print("Universal - requires curl and bsdtar (libarchive):")
    print("  $ curl https://gitlab.com/evlaV/jupiter-hw-support/-/archive/master/jupiter-hw-support-master.%s?path=usr/share/jupiter_bios_updater | bsdtar --strip-components=3 -xvf-" % random.choice(rel_bios_dl_pkg_nozip))
    print("  $ sudo jupiter_bios_updater/h2offt jupiter-%s-bios-backup.bin -O" % rel_bios_ver)
    print("=" * 24)
    print("\n%s\n" % ('-' * 12))
else:
    print("-h, --help to show description, source code, documentation, BIOS database/download/backup, and help (usage, examples, positional arguments, and options).\n")

parser = argparse.ArgumentParser(usage='%(prog)s [SOURCE_BIOS_IMAGE[.bin|.fd|.rom]]\n       [DESTINATION_BIOS_IMAGE[.bin|.rom]] [-h] [-d] [-g] [-i] [-r]\n\n e.g.: %(prog)s jupiter-' + rel_bios_ver + '-bios-backup.bin -d\n       %(prog)s ' + rel_bios_ver + '_sign.fd ' + rel_bios_ver + '-trimmed.bin -i')
parser.add_argument('src', metavar='SOURCE_BIOS_IMAGE[.bin|.fd|.rom]', nargs='?',
                    help='analyze/verify SOURCE BIOS image (e.g., %s)' % (rel_bios_ver + '_sign.fd'))
parser.add_argument('dest', metavar='DESTINATION_BIOS_IMAGE[.bin|.rom]', nargs='?',
                    help='dynamically trim SOURCE BIOS image and/or inject UID to DESTINATION (SOURCE -> DESTINATION)')
parser.add_argument('-d', '--dump-uid', dest='dump_uid', const=dump_uid_default_file, metavar='DUMP_UID_TO_FILE', nargs='?',
                    help='dump SOURCE UID to SPECIFIED file (default: %s)' % dump_uid_default_file)
parser.add_argument('-g', '--generate-uid', dest='generate_uid', const=generate_uid_default_file, metavar='GENERATE_UID_TO_FILE', nargs='?',
                    help='generate (pseudorandom) UID to SPECIFIED file (default: %s) and inject to DESTINATION (if SPECIFIED)' % generate_uid_default_file)
parser.add_argument('-i', '--inject-uid', dest='inject_uid', const=dump_uid_default_file, metavar='INJECT_UID_FROM_FILE', nargs='?',
                    help='inject UID from SPECIFIED file (default: %s) to DESTINATION' % dump_uid_default_file)
parser.add_argument('-r', '--remove-uid', dest='remove_uid', action='store_true',
                    help='remove UID from SOURCE to DESTINATION (commonize/sanitize/shareable)')
args = parser.parse_args()

# generate UID file - F7A(0115/0116/0118)
if args.generate_uid:
    # nondestructive/conforming (refuse to overwrite)
    #if os.path.isfile(args.generate_uid):
    # destructive/nonconforming (overwrites generate_uid_default_file)
    if os.path.isfile(args.generate_uid) and args.generate_uid != generate_uid_default_file:
        print("error: generated UID file (%s) already exists! refuse to overwrite.\n" % args.generate_uid)
        sys.exit(1)
    with open(args.generate_uid, 'wb') as gen_uid_of:
        gen_uid_of.write(b'$DMI')
        gen_uid_of.write(b'\x02\x07\x00\x11\x00')
        #if not mebx_letter or not isinstance(mebx_letter, str) or not mebx_letter.isalpha() or not len(mebx_letter) == 1 or not mebx_letter[0].isupper():
        if not mebx_letter or not isinstance(mebx_letter, str) or not mebx_letter.isalpha():
            #mebx_letter = str(chr(random.randint(ord('A'), ord('Z'))))
            mebx_letter = str(chr(random.randint(65, 90)))
        gen_uid_of.write(b'MEB' + bytes(mebx_letter[0].upper(), 'utf-8'))
        if not mebx_number or not isinstance(mebx_number, int):
            mebx_number = random.randint(0, 99999999)
        #mebx_number = '{:08d}'.format(mebx_number)
        mebx_number = str(mebx_number).zfill(8)
        if not mebx_alphanumeric or not mebx_alphanumeric.isalnum():
            mebx_alphanumeric = ''.join(random.choice(string.ascii_letters + string.digits) for i in range(8))
        mebx_alphanumeric = str(mebx_alphanumeric).zfill(8)
        #gen_uid_of.write(bytes(mebx_number, 'utf-8'))
        gen_uid_of.write(bytes(mebx_alphanumeric.upper(), 'utf-8'))
        gen_uid_of.write(b'\x02\x07\xff\x11\x00')
        gen_uid_of.write(b'MEB' + bytes(mebx_letter[0].upper(), 'utf-8'))
        #gen_uid_of.write(bytes(mebx_number, 'utf-8'))
        gen_uid_of.write(bytes(mebx_alphanumeric.upper(), 'utf-8'))
        gen_uid_of.write(b'\x01\x07\x00\x11\x00')
        #if not fxaa_letter or not isinstance(fxaa_letter, str) or not fxaa_letter.isalpha() or not len(fxaa_letter) == 1 or not fxaa_letter[0].isupper():
        if not fxaa_letter or not isinstance(fxaa_letter, str) or not fxaa_letter.isalpha():
            #fxaa_letter = str(chr(random.randint(ord('A'), ord('Z'))))
            fxaa_letter = str(chr(random.randint(65, 90)))
        gen_uid_of.write(b'F' + bytes(fxaa_letter[0].upper(), 'utf-8') + b'AA')
        if not fxaa_number or not isinstance(fxaa_number, int):
            fxaa_number = random.randint(0, 99999999)
        #fxaa_number = '{:08d}'.format(fxaa_number)
        fxaa_number = str(fxaa_number).zfill(8)
        if not fxaa_alphanumeric or not fxaa_alphanumeric.isalnum():
            fxaa_alphanumeric = ''.join(random.choice(string.ascii_letters + string.digits) for i in range(8))
        fxaa_alphanumeric = str(fxaa_alphanumeric).zfill(8)
        #gen_uid_of.write(bytes(fxaa_number, 'utf-8'))
        gen_uid_of.write(bytes(fxaa_alphanumeric.upper(), 'utf-8'))
        gen_uid_of.write(b'\x01\x07\xff\x11\x00')
        gen_uid_of.write(b'F' + bytes(fxaa_letter[0].upper(), 'utf-8') + b'AA')
        #gen_uid_of.write(bytes(fxaa_number, 'utf-8'))
        gen_uid_of.write(bytes(fxaa_alphanumeric.upper(), 'utf-8'))
        gen_uid_of.write(b'\x0b\x06\x00\x16\x00')
        #gen_uid_of.write(b'5.0')
        #gen_uid_of.write(b'5.555555555555555')
        gen_uid_of.write(b'5.' + b'5' * 15)
        gen_uid_of.write(b'\x0b\x06\xff\x16\x00')
        #gen_uid_of.write(b'5.0')
        #gen_uid_of.write(b'5.555555555555555')
        gen_uid_of.write(b'5.' + b'5' * 15)
        gen_uid_of.write(b'\x0b\x05\x00\x40\x00')
        gen_uid_of.write(bytes(uid_1, 'utf-8'))
        gen_uid_of.write(b'\x0b\x05\xff\x40\x00')
        gen_uid_of.write(bytes(uid_1, 'utf-8'))
        gen_uid_of.write(b'\x0b\x07\x00\x40\x00')
        gen_uid_of.write(bytes(uid_2, 'utf-8'))
        gen_uid_of.write(b'\x0b\x07\xff\x40\x00')
        gen_uid_of.write(bytes(uid_2, 'utf-8'))
        print("successfully generated UID file (%s). byte size (%s).\n" % (args.generate_uid, gen_uid_of.tell()))
        # inject with -i, --inject-uid
        #if args.inject_uid == dump_uid_default_file:
        # inject with/without -i, --inject-uid
        if args.inject_uid == dump_uid_default_file or not args.inject_uid:
            args.inject_uid = args.generate_uid
        if not args.src:
            sys.exit(0)

# open/read UID file
if args.inject_uid:
    if os.path.isfile(args.inject_uid):
        with open(args.inject_uid, 'rb') as uid_if:
            bios_uid_inject = uid_if.read()
    else:
        print("error: UID file (%s) does not exist!\n" % args.inject_uid)
        sys.exit(2)

# SOURCE BIOS image check
if not args.src:
    print("error: SOURCE BIOS image not SPECIFIED!\n")
    sys.exit(3)
if not os.path.isfile(args.src):
    print("error: SOURCE BIOS image (%s) does not exist!\n" % args.src)
    sys.exit(4)
if os.path.getsize(args.src) < bios_size:
    print("%s: error: less than BIOS byte size! | %s < %s | %s fewer bytes!\n%s: corrupt/invalid!\n" % (args.src, os.path.getsize(args.src), bios_size, os.path.getsize(args.src) - bios_size, args.src))
    #if args.dest:
    if args.dest and not args.dump_uid and not args.generate_uid and not args.inject_uid:
        print("abort!\n")
        sys.exit(5)
# moved / more effective below
#if os.path.getsize(args.src) == bios_size:
    ##if args.dest:
    #if args.dest and not args.dump_uid and not args.generate_uid and not args.inject_uid and not args.remove_uid:
        #print("%s: error: TRIMMED or BACKUP BIOS image! nothing to trim.\n" % args.src)
        #print("abort!\n")
        #sys.exit(6)
    #print("%s: TRIMMED or BACKUP BIOS image.\n" % args.src)
    ##sys.exit(6)
# detect RELEASE BIOS byte size
for drbv_size, drbv_str in zip(rel_bios_size_list[::2], rel_bios_size_list[1::2]):
    if os.path.getsize(args.src) == drbv_size:
        #print("%s: valid UNTRIMMED RELEASE BIOS byte size (%s) detected." % (args.src, drbv_size))
        print("%s: valid UNTRIMMED RELEASE BIOS image %s byte size (%s) detected." % (args.src, drbv_str, drbv_size))
        break

# read SOURCE BIOS image binary
with open(args.src, 'rb') as sf:
    sf_data = sf.read()
    # redundant?
    sf.seek(0)
    # detect BIOS image version
    if sf_data.find(bios_ver_offset_str) >= 0:
    #if sf_data.rfind(bios_ver_offset_str) >= 0:
        bios_ver_offset = sf_data.find(bios_ver_offset_str) - bios_ver_offset_str_negative_offset
        #bios_ver_offset = sf_data.rfind(bios_ver_offset_str) - bios_ver_offset_str_negative_offset
        if bios_ver_offset >= 0:
            sf.seek(bios_ver_offset)
            bios_ver = str(sf.read(bios_ver_size), 'utf-8')
            sf.seek(0)
            print("%s: BIOS image version (%s) detected at offset (%s)." % (args.src, bios_ver, hex(bios_ver_offset)))
    # detect BIOS image revision/date
    if sf_data.find(bios_rev_offset_str) >= 0:
    #if sf_data.rfind(bios_rev_offset_str) >= 0:
        bios_rev_offset = sf_data.find(bios_rev_offset_str) - bios_rev_offset_str_negative_offset
        if bios_rev_offset >= 0:
            sf.seek(bios_rev_offset)
            bios_rev = str(sf.read(bios_rev_size), 'utf-8')
            sf.seek(0)
            print("%s: BIOS image revision (%s) detected at offset (%s)." % (args.src, bios_rev, hex(bios_rev_offset)))
        bios_date_offset = sf_data.find(bios_rev_offset_str) - bios_date_offset_str_negative_offset
        if bios_date_offset >= 0:
            sf.seek(bios_date_offset)
            bios_date = str(sf.read(bios_date_size), 'utf-8')
            sf.seek(0)
            print("%s: BIOS image date (%s) detected at offset (%s)." % (args.src, bios_date, hex(bios_date_offset)))
    # rel_header (2)
    sf_header = sf.read(2)
    #sf.seek(0)
    # bios_header (5)
    #sf_bios_header = sf.read(5)
    # 2 + "3" = 5
    sf_bios_header = sf_header + sf.read(3)
    sf.seek(0)
    # header checks
    if sf_header == rel_header:
        print("%s: valid UNTRIMMED RELEASE BIOS header detected." % args.src)
    elif sf_bios_header == bios_header:
        #if args.dest:
        if args.dest and not args.dump_uid and not args.generate_uid and not args.inject_uid:
            print("%s: warning: valid TRIMMED or BACKUP BIOS header detected.\n" % args.src)
        else:
            print("%s: valid TRIMMED or BACKUP BIOS header detected." % args.src)
        if os.path.getsize(args.src) == bios_size:
            #if args.dest:
            if args.dest and not args.dump_uid and not args.generate_uid and not args.inject_uid and not args.remove_uid:
                print("%s: error: TRIMMED or BACKUP BIOS image! nothing to trim.\n" % args.src)
                print("abort!\n")
                sys.exit(6)
            if bios_ver:
                print("%s: TRIMMED or BACKUP BIOS image version %s.\n" % (args.src, bios_ver))
            else:
                print("%s: TRIMMED or BACKUP BIOS image.\n" % args.src)
            #sys.exit(6)
    else:
        #print("%s: warning: invalid/unknown header detected! | %s != %s\n" % (args.src, sf_header, header))
        print("%s: warning: invalid/unknown header detected!\n" % args.src)
    if sf_data.find(bios_offset_str) >= 0:
        #bios_offset = sf_data.find(bios_offset_str)
        bios_offset = sf_data.find(bios_offset_str) - bios_offset_str_negative_offset
        if bios_offset < 0:
            print("%s: error: corrupt/invalid BIOS header detected! | %s fewer bytes!\n" % (args.src, abs(bios_offset)))
            print("abort!\n")
            sys.exit(7)
        print("%s: BIOS offset (%s) detected." % (args.src, hex(bios_offset)))
        # detect BIOS image version
        for drbv_offset, drbv_str in zip(bios_offset_list[::2], bios_offset_list[1::2]):
            if bios_offset == drbv_offset:
                detect_rel_bios_ver = drbv_str
                break
            detect_rel_bios_ver = None
    else:
        print("%s: error: BIOS offset not detected!\n" % args.src)
        invalid = True
        # fallback to (preset) bios_offset_list
        for drbv_offset, drbv_str in zip(bios_offset_list[::2], bios_offset_list[1::2]):
            if isinstance(drbv_offset, int) and isinstance(drbv_str, str):
                print("using (fallback/preset) %s BIOS offset (%s)\n" % (drbv_str, hex(drbv_offset)))
                sf.seek(drbv_offset)
                if sf.read(5) == bios_header:
                    bios_offset = drbv_offset
                    invalid = None
                    break
                print("error: %s BIOS offset (%s) invalid!\n" % (drbv_str, hex(drbv_offset)))
    sf.seek(bios_offset)
    if sf.read(5) == bios_header:
        print("%s: valid BIOS header detected." % args.src)
    else:
        print("%s: error: invalid/unknown header detected!\ncontinuing, despite low success rate\n" % args.src)
        invalid = True
    # detect UID offset
    if sf_data.find(bios_uid_offset_str) >= 0:
    #if sf_data.rfind(bios_uid_offset_str) >= 0:
        bios_uid_offset = sf_data.find(bios_uid_offset_str)
        #bios_uid_offset = sf_data.rfind(bios_uid_offset_str)
        if bios_uid_offset >= 0:
            sf.seek(bios_uid_offset)
            sf_uid_data = sf.read()
            bios_uid_size = sf_uid_data.find(b'\xff' * 4)
            sf.seek(bios_uid_offset)
            bios_uid = sf.read(bios_uid_size)
            sf.seek(0)
            if not bios_uid or bios_uid == bios_uid_offset_str:
                if os.path.getsize(args.src) == bios_size:
                    #print("%s: warning: NO/NULL UID offset (%s) detected! DO NOT FLASH!" % (args.src, hex(bios_uid_offset)))
                    print("%s: warning: NO/NULL UID offset (%s) detected! DO NOT FLASH! INJECT UID (-i)" % (args.src, hex(bios_uid_offset)))
                # maybe use rel_bios_size_list
                elif os.path.getsize(args.src) > bios_size:
                    print("%s: NO/NULL UID offset (%s) detected. TRIM AND INJECT UID (-i) OR FLASH WITH H2OFFT!" % (args.src, hex(bios_uid_offset)))
            else:
                print("%s: UID offset (%s) detected. byte size (%s)." % (args.src, hex(bios_uid_offset), bios_uid_size))
                if args.dump_uid:
                    if os.path.isfile(args.dump_uid):
                        print("error: UID file (%s) already exists! refuse to overwrite.\n" % args.dump_uid)
                        print("abort!\n")
                        sys.exit(8)
                    with open(args.dump_uid, 'wb') as uid_df:
                        uid_df.write(bios_uid)
                    print("%s: successfully dumped UID file (%s)." % (args.src, args.dump_uid))
    #sf.seek(bios_offset)
    # inject UID file
    if args.inject_uid and os.path.isfile(args.src) and bios_uid_offset >= 0 and bios_uid_offset >= bios_offset:
        sf.seek(bios_offset)
        # potentially inflate/pad bios_uid_inject
        if bios_uid_size > len(bios_uid_inject):
            print("%s: inflating/padding UID byte size: %s -> %s | padding byte size (%s)." % (args.src, len(bios_uid_inject), bios_uid_size, bios_uid_size - len(bios_uid_inject)))
            # must be AFTER print()
            bios_uid_inject += b'\xff' * (bios_uid_size - len(bios_uid_inject))
        #bios = sf.read(bios_uid_offset - bios_offset)
        #bios += bios_uid_inject
        bios = sf.read(bios_uid_offset - bios_offset) + bios_uid_inject
        # relative seek(,1)
        sf.seek(len(bios_uid_inject), 1)
        if bios_size >= len(bios):
            #bios += sf.read(bios_size - (bios_uid_offset - bios_offset) - len(bios_uid_inject))
            bios += sf.read(bios_size - len(bios))
        else:
            print("error: injected BIOS (%s) greater than BIOS byte size! | %s > %s | %s more bytes!\ninjected BIOS (%s) corrupt/invalid!\n" % (len(bios), len(bios), bios_size, len(bios) - bios_size, len(bios)))
            sys.exit(9)
        print("%s: injected UID file (%s) at offset (%s) | byte size (%s)." % (args.src, args.inject_uid, hex(bios_uid_offset), len(bios_uid_inject)))
    # remove UID (inject blank UID)
    elif args.remove_uid:
        sf.seek(bios_offset)
        bios = sf.read(bios_uid_offset - bios_offset) + b'\xff' * bios_uid_size
        # relative seek(,1)
        sf.seek(bios_uid_size, 1)
        #bios += sf.read(bios_size - (bios_uid_offset - bios_offset) - len(bios_uid_inject))
        bios += sf.read(bios_size - len(bios))
    # RAW BIOS
    else:
        sf.seek(bios_offset)
        bios = sf.read(bios_size)

# BIOS byte size check
if len(bios) == bios_size:
    print("%s: valid BIOS byte size (%s) detected." % (args.src, len(bios)))
else:
    print("error: BIOS byte size (%s) mismatch! | %s != %s | %s byte size difference!\n%s: corrupt/invalid!\n" % (len(bios), len(bios), bios_size, len(bios) - bios_size, args.src))
    print("abort!\n")
    sys.exit(10)

if not invalid:
    if detect_rel_bios_ver:
        if bios_ver:
            print("%s: UNTRIMMED RELEASE BIOS image version %s | %s.\n" % (args.src, bios_ver, detect_rel_bios_ver))
        else:
            print("%s: UNTRIMMED RELEASE BIOS image version %s.\n" % (args.src, detect_rel_bios_ver))
    elif bios_ver:
        print("%s: BIOS image version %s.\n" % (args.src, bios_ver))
    else:
        if os.path.getsize(args.src) > bios_size:
            print("%s: invalid/unknown (UNTRIMMED RELEASE?) BIOS image!\n" % args.src)
        elif os.path.getsize(args.src) == bios_size:
            print("%s: invalid/unknown (TRIMMED or BACKUP?) BIOS image!\n" % args.src)
        else:
            print("%s: invalid/unknown BIOS image!\n" % args.src)

# exit if no DESTINATION BIOS image SPECIFIED
if not args.dest:
    sys.exit(0)

# DESTINATION BIOS image check
if os.path.isfile(args.dest):
    print("error: DESTINATION BIOS image (%s) already exists! refuse to overwrite.\n" % args.dest)
    print("abort!\n")
    sys.exit(11)

# write DESTINATION BIOS image binary
with open(args.dest, 'wb') as df:
    df.write(bios)

if invalid:
    print("%s: potentially trimmed: %s -> %s\n" % (args.dest, args.src, args.dest))
    print("%s: warning: potentially corrupt/invalid! manually check/verify integrity before use!\n" % args.dest)
    sys.exit(12)
else:
    print("%s: successfully trimmed: %s -> %s\n" % (args.dest, args.src, args.dest))
    sys.exit(0)
